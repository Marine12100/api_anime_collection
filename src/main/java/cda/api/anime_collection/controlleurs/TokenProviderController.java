package cda.api.anime_collection.controlleurs;

import cda.api.anime_collection.security.token.AuthenticationTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
public class TokenProviderController {
    AuthenticationTokenProvider authenticationTokenProvider;

    @Autowired
    public TokenProviderController(AuthenticationTokenProvider authenticationTokenProvider) {
        this.authenticationTokenProvider = authenticationTokenProvider;
    }

    @PostMapping("/signup")
    @ResponseBody
    String recuperationToken(@RequestBody String userMail) {
        return authenticationTokenProvider.getToken(userMail);
    }
}
