package cda.api.anime_collection.controlleurs;

import cda.api.anime_collection.dto.Anime;
import cda.api.anime_collection.services.AnimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/AnimeAPI")
public class AnimeController {
    AnimeService animeService;

    @Autowired
    public AnimeController(AnimeService animeService) {
        this.animeService = animeService;
    }

    //LIST ANIMES
    @GetMapping(value = "/animes")
    @ResponseBody
    public List<Anime> recuperationListeAnime() {
        return animeService.recupererListeAnime();
    }

    //ANIME
    @GetMapping(value = "/animes/{idAnime}")
    @ResponseBody
    public Anime recuperationAnime(@PathVariable(value = "idAnime") Integer idAnime) {
        return animeService.recupererAnime(idAnime);
    }

    //ANIME BY GENRE
    @GetMapping(value = "/animes/{idAnime}/{genre}")
    @ResponseBody
    public List<Anime> recuperationListeAnimeByGenre(@PathVariable(value = "genre") String genre, @PathVariable(value = "idAnime") Integer id) {
        return animeService.recupererListeAnimeByGenre(genre, id);
    }
}
