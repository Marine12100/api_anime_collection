package cda.api.anime_collection.exceptions;

public class AnimeServiceException extends RuntimeException {
    private final Integer statusCode;

    private final String message;

    public AnimeServiceException(Integer statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }
}
