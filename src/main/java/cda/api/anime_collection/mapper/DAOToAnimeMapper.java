package cda.api.anime_collection.mapper;

import cda.api.anime_collection.dto.Anime;
import cda.api.dao.dto.anime;

public class DAOToAnimeMapper {
    public static Anime mapAnime(anime animeDAO) {
        Anime anime = new Anime();

        anime.setID(animeDAO.getID());
        anime.setTitre(animeDAO.getTitre());
        anime.setNature(animeDAO.isNature());
        anime.setGenre(animeDAO.getGenre());
        anime.setStudio_animation(animeDAO.getStudio_animation());
        anime.setDate_sortie(animeDAO.getDate_sortie());
        anime.setSynopsis(animeDAO.getSynopsis());
        anime.setLien_image(animeDAO.getLien_image());
        anime.setLien_video(animeDAO.getLien_video());

        return anime;
    }
}
