package cda.api.anime_collection.security.token;

import cda.api.anime_collection.utils.RandomString;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.HashMap;

@Component
public class AuthenticationTokenProvider implements AuthenticationProvider {
    private HashMap<String, String> authenticatedusers;

    public AuthenticationTokenProvider() {
        this.authenticatedusers = new HashMap<>();
    }

    public String getToken(String userMail) {
        if (!this.authenticatedusers.containsKey(userMail)) {
            genereNouveauToken(userMail);
        }
        return this.authenticatedusers.get(userMail);
    }

    public void genereNouveauToken(String userMail) {
        this.authenticatedusers.put(userMail, RandomString.getRandomString());
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String apiKey = (String) authentication.getPrincipal();

        if (ObjectUtils.isEmpty(apiKey)) {
            throw new InsufficientAuthenticationException("No API key in request");
        } else {
            if (this.authenticatedusers.containsValue(apiKey)) {
                return new AuthenticationToken(apiKey, true);
            }
            throw new BadCredentialsException("API Key is invalid");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return AuthenticationToken.class.isAssignableFrom(authentication);
    }
}
