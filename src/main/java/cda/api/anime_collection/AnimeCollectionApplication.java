package cda.api.anime_collection;

import cda.api.dao.repositories.AnimeRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication(scanBasePackages = {"cda.api.anime_collection", "cda.api.dao"}, exclude = SecurityAutoConfiguration.class)
@EnableJpaRepositories(basePackageClasses= AnimeRepository.class)
@EntityScan("cda.api.dao.dto")
public class AnimeCollectionApplication {
	public static void main(String[] args) {
		SpringApplication.run(AnimeCollectionApplication.class, args);
	}
}
