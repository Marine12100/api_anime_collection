package cda.api.anime_collection.services;

import cda.api.anime_collection.dto.Anime;
import cda.api.anime_collection.enums.HttpStatus;
import cda.api.anime_collection.exceptions.AnimeServiceException;
import cda.api.anime_collection.mapper.DAOToAnimeMapper;
import cda.api.dao.repositories.AnimeRepository;
import cda.api.dao.dto.anime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnimeService {
    AnimeRepository dao;
    @Autowired
    public AnimeService(AnimeRepository dao) {
        this.dao = dao;
    }

    public List<Anime> recupererListeAnime() {
        List<Anime> listeAnimeMapped = new ArrayList<>();

        for(anime anime : dao.findAll()) {
            listeAnimeMapped.add(DAOToAnimeMapper.mapAnime(anime));
        }

        try {
            if (listeAnimeMapped.isEmpty()) {
                throw new AnimeServiceException(HttpStatus.SERVER_ERROR.getValue(), "liste d'animes est vide");
            }
        } catch (NullPointerException e) {
            throw new AnimeServiceException(HttpStatus.SERVER_ERROR.getValue(), e.getMessage());
        }

        return listeAnimeMapped;
    }

    public Anime recupererAnime(Integer idAnime) {
        try{
            if(idAnime == null) {
                throw new AnimeServiceException(HttpStatus.SERVER_ERROR.getValue(), "idAnime est null");
            }
            return DAOToAnimeMapper.mapAnime(dao.getById(idAnime));
        } catch (NullPointerException e) {
            throw new AnimeServiceException(HttpStatus.SERVER_ERROR.getValue(), e.getMessage());
        }
    }
    public List<Anime> recupererListeAnimeByGenre(String genreAnime, Integer idAnime) {
        List<Anime> listeAnimeMapped = new ArrayList<>();

        for(anime anime : dao.findThreeByGenre(genreAnime, idAnime)) {
            listeAnimeMapped.add(DAOToAnimeMapper.mapAnime(anime));
        }

        try {
            if (listeAnimeMapped.isEmpty()) {
                throw new AnimeServiceException(HttpStatus.SERVER_ERROR.getValue(), "liste d'animes par genre est vide");
            }
        } catch (NullPointerException e) {
            throw new AnimeServiceException(HttpStatus.SERVER_ERROR.getValue(), e.getMessage());
        }

        return listeAnimeMapped;
    }
}
