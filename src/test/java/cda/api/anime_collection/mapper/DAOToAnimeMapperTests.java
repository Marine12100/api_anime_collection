package cda.api.anime_collection.mapper;

import cda.api.anime_collection.dto.Anime;
import cda.api.dao.dto.anime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DAOToAnimeMapperTests {
    @Test
     void testMapAnime() {
        // Given
        anime animeDAO = mock(anime.class);
        when(animeDAO.getID()).thenReturn(1);
        when(animeDAO.getTitre()).thenReturn("Berserk");
        when(animeDAO.isNature()).thenReturn(false);
        when(animeDAO.getGenre()).thenReturn("Fantasy");

        // When
        Anime anime = DAOToAnimeMapper.mapAnime(animeDAO);

        // Then
        Assertions.assertEquals(anime.getID(), animeDAO.getID());
        Assertions.assertEquals(anime.getTitre(), animeDAO.getTitre());
        Assertions.assertEquals(anime.isNature(), animeDAO.isNature());
        Assertions.assertEquals(anime.getGenre(), animeDAO.getGenre());
    }
}
