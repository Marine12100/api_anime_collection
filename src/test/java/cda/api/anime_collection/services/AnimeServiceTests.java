package cda.api.anime_collection.services;

import cda.api.anime_collection.dto.Anime;
import cda.api.anime_collection.exceptions.AnimeServiceException;
import cda.api.dao.dto.anime;
import cda.api.dao.repositories.AnimeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AnimeServiceTests {

    @Mock
    AnimeRepository dao;

    @InjectMocks
    AnimeService sut;

    @Test
    void testRecupererListeAnime() {
        //GIVEN
        List<anime> dummyList = genereListeAnimeRepo();
        when(dao.findAll()).thenReturn(dummyList);

        //WHEN
        sut.recupererListeAnime();

        //THEN
        verify(dao).findAll();
    }
    @Test
    void testRecupererListeAnimeBaseVide() {
        //GIVEN
        List<anime> emptyList = new ArrayList<>();
        when(dao.findAll()).thenReturn(emptyList);

        //WHEN / THEN
        Assertions.assertThrows(AnimeServiceException.class, () -> sut.recupererListeAnime());
    }

    @Test
    void testRecupererListeAnimeByGenre() {
        //GIVEN
        List<anime> dummyList = genereListeAnimeRepo();
        when(dao.findThreeByGenre(anyString(), anyInt())).thenReturn(dummyList);

        //WHEN
        sut.recupererListeAnimeByGenre("genre", 1);

        //THEN
        verify(dao).findThreeByGenre("genre", 1);
    }
    @Test
    void testRecupererListeAnimeByGenreIntrouvable() {
        //GIVEN
        List<anime> emptyList = new ArrayList<>();
        when(dao.findThreeByGenre(anyString(), anyInt())).thenReturn(emptyList);

        //WHEN / THEN
        Assertions.assertThrows(AnimeServiceException.class, () -> sut.recupererListeAnimeByGenre(anyString(), anyInt()));
    }

    @Test
    void testRecupererUnAnime() {
        //GIVEN
        anime dummyAnime = genereAnimeRepo(1, "dummy");
        when(dao.getById(anyInt())).thenReturn(dummyAnime);

        //WHEN
        sut.recupererAnime(anyInt());

        //THEN
        verify(dao).getById(anyInt());
    }

    @Test
    void testRecupererUnAnimeIdNull() {
        Assertions.assertThrows(AnimeServiceException.class, () -> sut.recupererAnime(null));
    }


    private List<Anime> genereListeAnime() {
        List<Anime> createdList = new ArrayList<>();
        createdList.add(genereAnime(1, "Hunter X Hunter"));
        createdList.add(genereAnime(2, "Attack On Titan"));
        createdList.add(genereAnime(3, "Samourai Champloo"));
        createdList.add(genereAnime(4, "Trigun"));
        createdList.add(genereAnime(5, "GTO"));
        return createdList;
    }
    private Anime genereAnime(int id, String titre) {
        Anime anime = new Anime();
        anime.setID(id);
        anime.setTitre(titre);
        anime.setStudio_animation("studio");
        anime.setGenre("genre");
        anime.setNature(true);
        anime.setDate_sortie("03/04/2005");
        anime.setSynopsis("synopsis");
        anime.setLien_image("lien image");
        anime.setLien_video("lien video");
        return anime;
    }
    private List<anime> genereListeAnimeRepo() {
        List<anime> createdList = new ArrayList<>();
        createdList.add(genereAnimeRepo(1, "Hunter X Hunter"));
        createdList.add(genereAnimeRepo(2, "Attack On Titan"));
        createdList.add(genereAnimeRepo(3, "Samourai Champloo"));
        createdList.add(genereAnimeRepo(4, "Trigun"));
        createdList.add(genereAnimeRepo(5, "GTO"));
        return createdList;
    }
    private anime genereAnimeRepo(int id, String titre) {
        anime anime = new anime();
        anime.setID(id);
        anime.setTitre(titre);
        return anime;
    }

}
