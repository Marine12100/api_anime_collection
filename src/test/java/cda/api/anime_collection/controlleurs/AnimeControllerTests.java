package cda.api.anime_collection.controlleurs;

import cda.api.anime_collection.dto.Anime;
import cda.api.anime_collection.services.AnimeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class AnimeControllerTests {

    @Mock
    AnimeService animeService;

    @InjectMocks
    AnimeController sut;

    @Test
    void testRecuperationListeAnime() {
        //GIVEN
        List<Anime> expectedResult;
        expectedResult = genereListeAnime();
        Mockito.when(animeService.recupererListeAnime()).thenReturn(expectedResult);

        //WHEN
        List<Anime> retrievedAnimeList = sut.recuperationListeAnime();

        //THEN
        Assertions.assertEquals(expectedResult, retrievedAnimeList);
    }

    @Test
    void testRecuperationAnime() {
        //GIVEN
        Anime expectedResult = genereAnime(1, "GTO");
        Mockito.when(animeService.recupererAnime(1)).thenReturn(expectedResult);

        //WHEN
        Anime retrievedResult = sut.recuperationAnime(1);

        //THEN
        Assertions.assertSame("GTO", retrievedResult.getTitre());
        Assertions.assertEquals(1, retrievedResult.getID());
    }

    @Test
    void testRecuperationListeAnimeByGenre() {
        //GIVEN
        List<Anime> expectedResult;
        expectedResult = genereListeAnime();
        Mockito.when(animeService.recupererListeAnimeByGenre(Mockito.anyString(), Mockito.anyInt())).thenReturn(expectedResult);

        //WHEN
        List<Anime> retrievedAnimeList = sut.recuperationListeAnimeByGenre("genre", 1);

        //THEN
        Assertions.assertEquals(expectedResult, retrievedAnimeList);
    }
    private List<Anime> genereListeAnime() {
        List<Anime> createdList = new ArrayList<>();
        createdList.add(genereAnime(1, "Hunter X Hunter"));
        createdList.add(genereAnime(2, "Attack On Titan"));
        createdList.add(genereAnime(3, "Samourai Champloo"));
        createdList.add(genereAnime(4, "Trigun"));
        createdList.add(genereAnime(5, "GTO"));
        return createdList;
    }
    private Anime genereAnime(int id, String titre) {
        Anime anime = new Anime();
        anime.setID(id);
        anime.setTitre(titre);
        anime.setStudio_animation("studio");
        anime.setGenre("genre");
        anime.setNature(true);
        anime.setDate_sortie("03/04/2005");
        anime.setSynopsis("synopsis");
        anime.setLien_image("lien image");
        anime.setLien_video("lien video");
        return anime;
    }
}
