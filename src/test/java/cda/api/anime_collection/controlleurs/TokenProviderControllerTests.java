package cda.api.anime_collection.controlleurs;

import cda.api.anime_collection.dto.Anime;
import cda.api.anime_collection.security.config.SecurityConfig;
import cda.api.anime_collection.security.token.AuthenticationTokenProvider;
import cda.api.anime_collection.services.AnimeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static cda.api.anime_collection.utils.RandomString.getRandomString;

@ExtendWith(MockitoExtension.class)
public class TokenProviderControllerTests {
    @Mock
    AuthenticationTokenProvider authenticationTokenProvider;

    @InjectMocks
    TokenProviderController sut;

    @Test
    void testRecuperationToken() {
        //GIVEN
        String mail = "test@test.com";
        String expectedToken;
        expectedToken = getRandomString();
        Mockito.when(authenticationTokenProvider.getToken(mail)).thenReturn(expectedToken);

        //WHEN
        String retrievedToken = sut.recuperationToken(mail);

        //THEN
        Assertions.assertEquals(expectedToken, retrievedToken);
    }
}
