FROM openjdk:11-jdk

VOLUME /tmp

COPY ./target/* .
EXPOSE 8080

CMD java -jar /anime_collection-0.0.1-SNAPSHOT.jar
